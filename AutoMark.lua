if RequiredScript == "lib/units/beings/player/states/playerstandard" then

	PlayerStandard.VANILLA_MODE = false		--Adhere to default rules
	PlayerStandard.DEBUG = false

	local init_original = PlayerStandard.init
	local update_original = PlayerStandard.update
	local _update_fwd_ray_original = PlayerStandard._update_fwd_ray

	function PlayerStandard:init(...)
		self._debug_units_marked = {}
		self._next_shout_t = 0
		self._steelsight_highlight = managers.player:has_category_upgrade("weapon", "steelsight_highlight_specials")
		self._marking_time_multiplier = managers.player:upgrade_value("player", "mark_enemy_time_multiplier", 1)
	
		self._mark_types = {
			enemy = managers.player:get_contour_for_marked_enemy(),
			civilian = managers.player:get_contour_for_marked_enemy(),
			swat_turret = managers.player:get_contour_for_marked_enemy("swat_turret"),
			camera = "mark_unit",
		}
		
		self._shout_types = {
			--Enemies and civilians governed by character tweak data entry
			swat_turret = "f44x_any",
			camera = "f39_any",
		}
		
		local range_base = tweak_data.player.long_dis_interaction.highlight_range
		local static_range_base = 2000
		local range_mul = 
			managers.player:upgrade_value("player", "marked_distance_mul", 1) * 
			managers.player:upgrade_value("player", "intimidate_range_mul", 1) * 
			managers.player:upgrade_value("player", "passive_intimidate_range_mul", 1)
		
		self._mark_ranges = {
			enemy = range_base * range_mul,
			civilian = range_base * range_mul,
			camera = static_range_base,
			swat_turret = static_range_base,
		}
		
		self._mask_off_mark_enabled = {
			enemy = managers.player:has_category_upgrade("player", "special_enemy_highlight_mask_off"),
			civilian = managers.player:has_category_upgrade("player", "special_enemy_highlight_mask_off"),
			swat_turret = managers.player:has_category_upgrade("player", "special_enemy_highlight_mask_off"),
			camera = managers.player:has_category_upgrade("player", "sec_camera_highlight_mask_off"),
		}
		
		return init_original(self, ...)
	end

	function PlayerStandard:update(t, dt, ...)
		update_original(self, t, dt, ...)
		
		self._clear_debug_marks = {}
		
		if self._fwd_ray_new then
			local state = self._unit:movement():current_state_name()
			local action_forbidden = self:chk_action_forbidden("interact") 
				or self._unit:base():stats_screen_visible()
				or self:_interacting()
				or self._ext_movement:has_carry_restriction()
				or self:is_deploying()
				or self:_changing_weapon()
				or self:_is_throwing_grenade()
				or self:_is_meleeing()
				or self:_on_zipline()
				or state == "incapacitated"
				or state == "tased"
				or state == "civilian"
					
			if not action_forbidden then
				self:_auto_mark_units(t, dt)
			end
		end
		
		for _, data in ipairs(self._clear_debug_marks) do
			data.unit:contour():remove(data.mark)
		end
	end
	
	--Override, replaces default auto-mark and ranging mechanism
	function PlayerStandard:_update_fwd_ray(...)
		if self._fwd_ray_new then
			local from = self._unit:movement():m_head_pos()
			local to = self._cam_fwd * 4000
			mvector3.add(to, from)
			self._fwd_ray = World:raycast("ray", from, to, "slot_mask", self._slotmask_fwd_ray)
			managers.environment_controller:set_dof_distance(math.max(0, (self._fwd_ray and self._fwd_ray.distance or 4000) - 200), self._state_data.in_steelsight)
			return
		end
		
		return _update_fwd_ray_original(self, ...)
	end
	
	
	function PlayerStandard:_auto_mark_units(t, dt)
		local in_steelsight = self._state_data.in_steelsight
		local stealth = managers.groupai:state():whisper_mode()
		local ranging_scope = alive(self._equipped_unit) and self._equipped_unit:base():has_range_distance_scope()
		local debug_units_marked = {}
		
		--Update ranging scope to account for transparent world objects
		if alive(self._equipped_unit) and self._equipped_unit:base().set_scope_range_distance then
			local distance = self._fwd_ray_new:get_distance()
			self._equipped_unit:base():set_scope_range_distance(distance and (distance/100) or false)
		end
		
		--Check auto-marking of units
		for _, data in ipairs(self._fwd_ray_new:get_targets_by_type({ "camera", "enemy", "civilian", "swat_turret" }, true)) do
			if alive(data.unit) then
				local unit = data.unit
				
				if PlayerStandard.DEBUG then
					local debug_mark = string.format("mark_%s_flash", (data.type == "camera" or data.type == "swat_turret") and "unit" or data.type)
					debug_units_marked[unit:key()] = { unit = unit, mark = debug_mark }
					
					if not self._debug_units_marked[unit:key()] then
						unit:contour():add_safe(debug_mark)
					end
				end
			
				if PlayerStandard.VANILLA_MODE then
					if in_steelsight and data.distance <= (ranging_scope and 20000 or 4000) and self._equipped_unit:base().check_highlight_unit then
						self._equipped_unit:base():check_highlight_unit(unit)
					end
				
					break
				end
				
				local can_mark = not (self._ext_movement:current_state_name() == "mask_off") or self._mask_off_mark_enabled[data.type]
				local range = self._mark_ranges[data.type] or 0
				
				if (ranging_scope or self._steelsight_highlight) and in_steelsight then
					range = math.max(range, ranging_scope and 20000 or 4000)
				end
				
				if can_mark and data.distance <= range then
					local shout_type = self._shout_types[data.type]
					local char_tweak = unit:base().char_tweak and unit:base():char_tweak()
					
					if data.type == "camera" then
						shout_type = stealth and not unit:base():destroyed() and self._shout_types.camera
					elseif data.type == "swat_turret" then
						shout_type = self._shout_types.swat_turret
					elseif char_tweak then
						if stealth then
							shout_type = char_tweak.silent_priority_shout and (char_tweak.silent_priority_shout .. "_any")
						else
							shout_type = char_tweak.priority_shout and (char_tweak.priority_shout .. "x_any")
						end
					end
					
					if shout_type then
						if unit:contour():add_safe(self._mark_types[data.type], true, self._marking_time_multiplier) then
							if t > self._next_shout_t then
								self._next_shout_t = t + math.random(10, 15)
								self:say_line(shout_type, true)
							end
						end
					end
				end
			end
		end
		
		if PlayerStandard.DEBUG then
			for key, old_marks in pairs(self._debug_units_marked) do
				if not debug_units_marked[key] and alive(old_marks.unit) then
					table.insert(self._clear_debug_marks, old_marks)
				end
			end
			
			self._debug_units_marked = debug_units_marked
		end
	end	
end

if RequiredScript == "lib/units/contourext" then

	local init_original = ContourExt.init
	local add_original = ContourExt.add
	local remove_original = ContourExt.remove

	function ContourExt:init(...)
		self._next_mark_allowed_t = {}
		self._queued_markings = {}
		self._queued_types = {}
		self._last_mark_t = 0
		return init_original(self, ...)
	end
	
	function ContourExt:add(type, ...)
		self._last_mark_t = TimerManager:game():time()
		return add_original(self, type, ...)
	end

	function ContourExt:remove(type, ...)
		self._next_mark_allowed_t[type] = nil
		if self._queued_types[type] then
			self._queued_types[type] = nil
			for i, data in ipairs(self._queued_markings) do
				if data[1] == type then
					table.remove(self._queued_markings, i)
					break
				end
			end
		end
		
		return remove_original(self, type, ...)
	end
	
	
	function ContourExt:add_safe(type, sync, multiplier)
		local t = TimerManager:game():time()
		
	function ContourExt:add_safe(type, sync, multiplier)
		local t = TimerManager:game():time()
		
		if self._queued_types[type] then
			--printf("(%.2f) %s already queued", t, type)
			return
		elseif t < (self._next_mark_allowed_t[type] or 0) then
			--printf("(%.2f) %s too soon for new mark (%.2f)", t, type, (self._next_mark_allowed_t[type] or 0))
			return
		end
		
		if t <= self._last_mark_t then
			--printf("Queue: %s", type)
			self._queued_types[type] = true
			table.insert(self._queued_markings, { type or false, sync or false, multiplier or false })
			if not self._queue_callback then
				self._queue_callback = "check_contour_queue" .. tostring(self._unit:key())
				managers.enemy:add_delayed_clbk(self._queue_callback, callback(self, self, "_check_queue"), 0)
			end
		else
			local setup = self:add(type, sync, multiplier)
			if setup then
				--printf("Added: %s", type)
				self._next_mark_allowed_t[type] = setup.fadeout_t and ((setup.fadeout_t + t) / 2) or (t + 3600)
			end
		end
		
		return true
	end
		
		if t <= self._last_mark_t then
			self._queued_types[type] = true
			table.insert(self._queued_markings, { type or false, sync or false, multiplier or false })
			self:_add_queue_callback()
		else
			local setup = self:add(type, sync, multiplier)
			if setup then
				self._next_mark_allowed_t[type] = setup.fadeout_t and ((setup.fadeout_t + t) / 2) or (t + 3600)
			end
		end
		
		return true
	end
	
	function ContourExt:_check_queue()
		--printf("Purge queue")
		self._queue_callback = nil
		
		if #self._queued_markings > 0 then
			local type, sync, multiplier = unpack(table.remove(self._queued_markings, 1))
			self._queued_types[type] = nil
			self:add_safe(type, sync, multiplier)
		end
		
		self:_add_queue_callback()
	end
	
	function ContourExt:_add_queue_callback()
		if not self._queue_callback and #self._queued_markings > 0 then
			self._queue_callback = "check_contour_queue" .. tostring(self._unit:key())
			managers.enemy:add_delayed_clbk(self._queue_callback, callback(self, self, "_check_queue"), 0)
		end
	end
	
	ContourExt._types["mark_civilian_flash"] = 	{ color = Vector3(0.5, 1.0, 0.5), priority = 1, material_swap_required = true }
	ContourExt._types["mark_enemy_flash"] = 		{ color = Vector3(1.0, 0.3, 1.0), priority = 1, material_swap_required = true }
	ContourExt._types["mark_unit_flash"] =			{ color = Vector3(1.0, 0.3, 1.0), priority = 1 }

end
















do return end

if string.lower(RequiredScript) == "lib/units/contourext" then

	ContourExt._ALLOWED_SHOUT_THRESHOLD = 10
	
	local init_original = ContourExt.init
	local remove_original = ContourExt.remove

	function ContourExt:init(...)
		self._mark_expire_t = {}
		self._last_mark_t = 0
		self._next_allowed_shout_t = 0
		return init_original(self, ...)
	end
	
	function ContourExt:remove(mark_type, ...)
		self._mark_expire_t[mark_type] = nil
		return remove_original(self, mark_type, ...)
	end

	function ContourExt:enqueue(t, mark_type, sync, pstate, unit_type)
		if t > (self._mark_expire_t[mark_type] or 0) and t > self._last_mark_t then
			self:add(mark_type, sync, managers.player:upgrade_value("player", "mark_enemy_time_multiplier", 1))
			self._mark_expire_t[mark_type] = t + (ContourExt._types[mark_type].fadeout or 3600) * 0.75
			self._last_mark_t = t
			if pstate and t >= self._next_allowed_shout_t and pstate:on_auto_mark(t, mark_type, self._unit, unit_type) then
				self._next_allowed_shout_t = t + ContourExt._ALLOWED_SHOUT_THRESHOLD
			end
		end
	end
	
elseif string.lower(RequiredScript) == "lib/units/beings/player/states/playerstandard" then

	PlayerStandard._THEIA_ONLY = false
	PlayerStandard._LOUD_MARKING_TIME_THRESHOLD = 0.15
	PlayerStandard._STEALTH_MARKING_TIME_THRESHOLD = 0.15
	PlayerStandard._MARKING_DECAY_FACTOR = 0.25
	PlayerStandard.AUTO_SHOUT_COOLDOWN = PlayerStandard.AUTO_SHOUT_COOLDOWN or 3
	
	local init_original = PlayerStandard.init
	local update_original = PlayerStandard.update
	local _update_fwd_ray_original = PlayerStandard._update_fwd_ray
	
	function PlayerStandard:init(...)
		self._last_shout_t = 0
		self._intimidate_t = 0
		self._active_markings = {}
		
		self._mark_types = {
			enemy = managers.player:get_contour_for_marked_enemy(),
			civilian = managers.player:get_contour_for_marked_enemy(),
			swat_turret = managers.player:get_contour_for_marked_enemy("swat_turret"),
			camera = "mark_unit",
		}
		
		self._shout_types = {
			--Enemies and civilians governed by character tweak data entry
			swat_turret = "f44x_any",
			camera = "f39_any",
			default = "f39_any",
		}
		
		local range_base = tweak_data.player.long_dis_interaction.highlight_range
		local static_range_base = 2000
		local range_mul = 
			managers.player:upgrade_value("player", "marked_distance_mul", 1) * 
			managers.player:upgrade_value("player", "intimidate_range_mul", 1) * 
			managers.player:upgrade_value("player", "passive_intimidate_range_mul", 1)
		
		self._mark_ranges = {
			enemy = range_base * range_mul,
			civilian = range_base * range_mul,
			camera = static_range_base,
			swat_turret = static_range_base,
		}
		
		self._mask_off_mark_enabled = {
			enemy = managers.player:has_category_upgrade("player", "special_enemy_highlight_mask_off"),
			civilian = managers.player:has_category_upgrade("player", "special_enemy_highlight_mask_off"),
			swat_turret = managers.player:has_category_upgrade("player", "special_enemy_highlight_mask_off"),
			camera = managers.player:has_category_upgrade("player", "sec_camera_highlight_mask_off"),
		}
		
		return init_original(self, ...)
	end

	function PlayerStandard:update(t, dt, ...)
		update_original(self, t, dt, ...)
		
		if self._fwd_ray_new then
			local state = self._unit:movement():current_state_name()
			local action_forbidden = self:chk_action_forbidden("interact") 
				or self._unit:base():stats_screen_visible()
				or self:_interacting()
				or self._ext_movement:has_carry_restriction()
				or self:is_deploying()
				or self:_changing_weapon()
				or self:_is_throwing_grenade()
				or self:_is_meleeing()
				or self:_on_zipline()
				or state == "incapacitated"
				or state == "tased"
				or state == "civilian"
					
			if not action_forbidden then
				self:_auto_mark_units(t, dt)
			end
		end
	end
	
	--Override, removes Theia marking code and replaces with auto-mark and new forward ray-based ranging
	function PlayerStandard:_update_fwd_ray(...)
		if self._fwd_ray_new then
			local from = self._unit:movement():m_head_pos()
			local to = self._cam_fwd * 4000
			mvector3.add(to, from)
			self._fwd_ray = World:raycast("ray", from, to, "slot_mask", self._slotmask_fwd_ray)
			managers.environment_controller:set_dof_distance(math.max(0, (self._fwd_ray and self._fwd_ray.distance or 4000) - 200), self._state_data.in_steelsight)
			
			if alive(self._equipped_unit) and self._equipped_unit:base().set_scope_range_distance then
				local distance = self._fwd_ray_new:get_distance()
				self._equipped_unit:base():set_scope_range_distance(distance and (distance/100) or false)
			end
			
			return
		end
		
		return _update_fwd_ray_original(self, ...)
	end
	
	function PlayerStandard:on_auto_mark(t, mark_type, unit, unit_type)
		if math.max(self._last_shout_t, self._intimidate_t) + PlayerStandard.AUTO_SHOUT_COOLDOWN < t then
			local stealth = managers.groupai:state():whisper_mode()
			local sound_name = self._shout_types[unit_type]
			if not sound_name then
				local char_tweak = tweak_data.character[unit:base()._tweak_table]
				sound_name = char_tweak and (stealth and char_tweak.silent_priority_shout .. "_any" or char_tweak.priority_shout .. "x_any") or self._shout_types.default
			end

			if sound_name then
				local gesture = self._ext_movement:current_state_name() ~= "mask_off" and "cmd_point"
				self:_do_action_intimidate(t, gesture, sound_name, false)
				self._last_shout_t = t
				self._intimidate_t = t - tweak_data.player.movement_state.interaction_delay * 0.5
				return true
			end
		end
	end
	
	function PlayerStandard:_auto_mark_units(t, dt)
		local function mark_unit(unit, stealth, type)
			if type == "enemy" or type == "civilian" then
				local char_tweak = unit:base():char_tweak()
				if not (stealth and char_tweak.silent_priority_shout or char_tweak.priority_shout) then
					return
				end
			end
			
			if unit:contour() then
				unit:contour():enqueue(t, self._mark_types[type], true, self, type)
			end
		end
		
		local mask_off_state = self._unit:movement():current_state_name() == "mask_off"
		local ranging_scope = self._state_data.in_steelsight and alive(self._equipped_unit) and self._equipped_unit:base():has_range_distance_scope()
		local stealth = managers.groupai:state():whisper_mode()
		local time_threshold = ranging_scope and 0 or stealth and PlayerStandard._STEALTH_MARKING_TIME_THRESHOLD or PlayerStandard._LOUD_MARKING_TIME_THRESHOLD
		local hit_units = {}
		local remove_keys = {}
		
		for _, data in ipairs(self._fwd_ray_new:get_targets_by_type({ "camera", "enemy", "civilian", "swat_turret" }, true)) do
			local is_camera = data.type == "camera"
			local can_mark = not mask_off_state or self._mask_off_mark_enabled[data.type]
			
			if can_mark and (not is_camera or stealth and not data.unit:base():destroyed()) then
				if PlayerStandard._THEIA_ONLY then
					if ranging_scope then
						mark_unit(data.unit, stealth, data.type)
					end
				else
					local key = data.unit:key()
					hit_units[key] = data
					self._active_markings[key] = self._active_markings[key] or 0
				end
			end
		end
		
		for key, t in pairs(self._active_markings) do
			local data = hit_units[key]
			local accumulation_mult = data and 1 or -PlayerStandard._MARKING_DECAY_FACTOR
			self._active_markings[key] = math.min(self._active_markings[key] + dt * accumulation_mult, time_threshold)
		
			if self._active_markings[key] < 0 then
				table.insert(remove_keys, key)
			elseif data and self._active_markings[key] >= time_threshold then
				if data.distance <= (ranging_scope and 20000 or self._mark_ranges[data.type]) then
					mark_unit(data.unit, stealth, data.type)
				end
			end
		end
		
		for _, key in ipairs(remove_keys) do
			self._active_markings[key] = nil
		end
	end
	
	PlayerStandard.USING_AUTOMARK = true --External flag

end